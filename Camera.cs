﻿/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
namespace Project
{
    public class Camera
    {
        public Matrix View, ViewRH;
        public Matrix Projection, ProjectionRH;
        public LabGame game;
        public Vector3 pos;
        public Vector3 oldPos;

        // Ensures that all objects are being rendered from a consistent viewpoint
        public Camera(LabGame game) {
            pos = new Vector3(0, 10, -7);
            View = Matrix.LookAtLH(pos, new Vector3(0, 0, 0), Vector3.UnitY);
            ViewRH = Matrix.LookAtRH(pos, new Vector3(0, 0, 0), Vector3.UnitY);
            Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
            ProjectionRH = Matrix.PerspectiveFovRH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
            this.game = game;
        }

        // If the screen is resized, the projection matrix will change
        public void Update()
        {
            Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
            ProjectionRH = Matrix.PerspectiveFovRH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
            View = Matrix.LookAtLH(pos, new Vector3(0, 0, 0), Vector3.UnitY);
            ViewRH = Matrix.LookAtRH(pos, new Vector3(0, 0, 0), Vector3.UnitY);
            if(game.player != null)
            {
                this.pos.X = game.player.position.X;
                //this.pos.Z = game.player.position.Z - 10;
                this.pos.Z = game.player.position.Z - 7;
                View = Matrix.LookAtLH(pos, game.player.position, Vector3.UnitY);
                ViewRH = Matrix.LookAtRH(pos, game.player.position, Vector3.UnitY);
            }
        }
    }
}
