// Copyright (c) 2010-2012 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Adapted for COMP30019 by Jeremy Nicholson, 10 Sep 2012
// Adapted further by Chris Ewin, 23 Sep 2013
// Adapted further by Jason Lee, Brian Kang, and Minh Thong, 23 Oct 2014

// these won't change in a given iteration of the shader
float4x4 World;
float4x4 View;
float4x4 Projection;
float4 cameraPos;
float4 lightAmbCol = float4(0.4f, 0.4f, 0.4f, 1.0f);
float4 lightPntPos;
float4 lightPntCol = float4(1.0f, 0.0f, 0.75f, 1.0f);
float4x4 worldInvTrp;
float3 playerPos;
bool isMerging;
float radius;
float intensity;

// variables for the plane texture and the bump map
texture2D ModelTexture;
texture2D xWaterBumpMap;
SamplerState textureSampler;

// variables to control the waves
float xWaveLength;
float xWaveHeight;
float xTime;
float3 xWindDirection;
float xWindForce;
//

struct VS_IN
{
	float4 pos : SV_POSITION;
	float4 nrm : NORMAL;
	float2 TextureCoordinate : TEXCOORD0;
// Other vertex properties, e.g. texture co-ords, surface Kd, Ks, etc
};

struct PS_IN
{
	float4 pos : SV_POSITION; //Position in camera co-ords
	float4 col : COLOR;
	float4 wpos : TEXCOORD0; //Position in world co-ords
	float3 wnrm : TEXCOORD1; //Normal in world co-ords
	float2 TextureCoordinate : TEXCOORD2;
	float2 BumpMapSamplingPos : TEXCOORD3;
};


PS_IN VS( VS_IN input )
{
	PS_IN output = (PS_IN)0;

	// Convert Vertex position and corresponding normal into world coords
	// Note that we have to multiply the normal by the transposed inverse of the world 
	// transformation matrix (for cases where we have non-uniform scaling; we also don't
	// care about the "fourth" dismension, because translations don't affect the normal)
	output.wpos = mul(input.pos, World);
	output.wnrm = mul(input.nrm.xyz, (float3x3)worldInvTrp);

	// Transform vertex in world coordinates to camera coordinates
	float4 viewPos = mul(output.wpos, View);
    output.pos = mul(viewPos, Projection);

	// Just pass along the colour at the vertex
	output.col = float4(0.5f, 0.5f, 0.5f, 1.0f);

	// Just pass along the coordinate of the texture
	output.TextureCoordinate = input.TextureCoordinate;

	float2 moveVector = float2(0, xTime*xWindForce);
	output.BumpMapSamplingPos = (input.TextureCoordinate + moveVector);
	//output.BumpMapSamplingPos = (input.TextureCoordinate);
	
	// code to change the direction of the waves
	/*
	float3 windDir = normalize(xWindDirection);
	float3 perpDir = cross(xWindDirection, float3(0, 1, 0));
	float ydot = dot(input.TextureCoordinate, xWindDirection.xz);
	float xdot = dot(input.TextureCoordinate, perpDir.xz);
	float2 moveVector = float2(xdot, ydot);
	moveVector.y += xTime*xWindForce;
	output.BumpMapSamplingPos = moveVector;
	*/
	return output;
}

float4 PS(PS_IN input) : SV_Target
{
	float4 returnCol = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float3 amb = float3(0.0f, 0.0f, 0.0f);
	float3 dif = float3(0.0f, 0.0f, 0.0f);
	float3 spe = float3(0.0f, 0.0f, 0.0f);
	float Ka, Kd, Ks, fAtt;
	float4 lightPos;

	// ripple effect of the plane

	// get 'pertubations' from the bump map
	float4 bumpColor = xWaterBumpMap.Sample(textureSampler, input.BumpMapSamplingPos);
	// syntax for DX9
	//float4 textureColor = tex2D(textureSampler, input.TextureCoordinate);
	
	float2 perturbation = xWaveHeight*(bumpColor.rg - 0.5f)*2.0f;
	float2 perturbatedTexCoords = input.TextureCoordinate + perturbation;

	// sample real texture with the perturbated uv coordinates
	float4 textureColor = ModelTexture.Sample(textureSampler, perturbatedTexCoords);
	
	if (!isMerging)
	{
		// Calculate ambient RGB intensities
		Ka = 0.0f;

		float dist = distance(input.wpos.xyz, lightPntPos.xyz);
		// Calculate diffuse RBG reflections
		fAtt = intensity*(1 - saturate(dist/8.0f));
		Kd = 1.0f;
		// Calculate specular reflections
		Ks = 0.25f;

		lightPos = lightPntPos;

	}
	// player is merging; display circle of light
	else
	{
		// r is the distance from the player to the current point on the plane
		float r = distance(input.wpos.xyz, playerPos);

		// only pixels at a certain distance from the player get lit
		if (r >= radius && r <(radius + 1.0f)){
			Ka = 0.0f;
			fAtt = 1;

			Kd = 0.5f;
			Ks = 0.25f;

			lightPos = input.wpos;
			lightPos.y += 1.0f;

			//amb = lightPntCol/2.0f;
		}
		else{
			Ka = 0.0f;
			fAtt = 1;

			Kd = 0.0f;
			Ks = 0.0f;

			lightPos = lightPntPos;
		}
	}

	// Our interpolated normal might not be of length 1
	float3 interpNormal = normalize(input.wnrm);

	// Calculate ambient RGB intensities
	amb = input.col.rgb*lightAmbCol.rgb*Ka;
	//float dist = distance(input.wpos.xyz, lightPos.xyz);
	// Calculate diffuse RBG reflections
	float3 L = normalize(lightPos.xyz - input.wpos.xyz);
	float LdotN = saturate(dot(L, interpNormal.xyz));
	dif = fAtt*lightPntCol.rgb*Kd*input.col.rgb*LdotN;

	// Calculate specular reflections
	float specN = 5; // Numbers>>1 give more mirror-like highlights
	float3 V = normalize(cameraPos.xyz - input.wpos.xyz);
	float3 R = normalize(2 * LdotN*interpNormal.xyz - L.xyz);
	//float3 R = normalize(0.5*(L.xyz+V.xyz)); //Blinn-Phong equivalent
	spe = fAtt*lightPntCol.rgb*Ks*pow(saturate(dot(V, R)), specN);

	returnCol.rgb = (textureColor.rgb * input.col.rgb) + amb.rgb + dif.rgb + spe.rgb;
	returnCol.a = input.col.a;
	return returnCol;
}

technique Lighting
{
    pass Pass1
    {
		Profile = 9.1;
        VertexShader = VS;
        PixelShader = PS;
    }
}