﻿/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

// This script controls the behavior of the enemy (AI) using SteeringManager class

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit;
using SharpDX;
using Windows.UI.Input;
using Windows.UI.Core;

namespace Project
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    class Enemy : GameObject
    {
        private static string modelName = "sphere";

        SteeringManager steering;

        public Enemy(LabGame game, Vector3 pos, float size)
        {
            this.game = game;
            type = GameObjectType.Enemy;
            this.size = size;

            GetParams(modelName);
            this.position = pos;
            steering = new SteeringManager(this, game);
            this.velocity = new Vector3(0, 0, 0);
        }

        public override void Update(GameTime gameTime)
        {
            if (game.player.isMerging) { return; }

            foreach (var obj in game.gameObjects)
            {
                // check the collision of enemies ( potential spawn on each other)
                if (obj.type == GameObjectType.Enemy && obj != this && ((((GameObject)obj).position - position).LengthSquared() <=
                    Math.Pow(((GameObject)obj).collisionRadius +
                    this.collisionRadius, 2)))
                {

                    if ((this.velocity == Vector3.Zero) || (obj.velocity == Vector3.Zero))
                    {
                        // This method works perfect when two enemy spawn overlapping with low speed
                        steering.flee(obj.position);
                    }
                    else
                    {   // This method seems behave more realistic when two enemy sphere collide with high speed
                        this.velocity.X = -this.velocity.X;
                        this.velocity.Z = -this.velocity.Z;

                    }

                }
            }

            // check if the enemy can see the player within its sight range
            if (Vector3.Distance(game.player.position, this.position) < GameObject.DEFAULT_SIGHT_DISTANCE+this.collisionRadius)
            {
                // for smaller enemies
                if (game.player.size > this.size)
                {
                    if(game.isAIEasy)
                    {
                        // normal flee
                        steering.flee(game.player.position);
                    }
                    else
                    {
                        // advance flee
                        steering.evade(game.player.position,game.player.velocity);
                    }
                    this.color = GameObject.FLEEING_ENEMY_COL;
                     
                }
                // for bigger enemies
                else
                {
                    if(game.isAIEasy)
                    {
                        // normal seek
                        steering.seek(game.player.position);
                    }
                    else
                    {
                        // advance seek
                        steering.pursuit(game.player.position,game.player.velocity);
                    }
                    this.color = GameObject.SEEKING_ENEMY_COL;
                     
                }
            }
            else
            {
                // otherwise wandering
                this.color = GameObject.WANDERING_ENEMY_COL;
                steering.wander();
            }

            // combine all that behavior to a single vector
            steering.update();

            // then reset the steering force for next update
            steering.reset();

            //Keep enemy within the boundaries.
            if (position.X - this.collisionRadius < game.plane.boundaryLeft) { 
                position.X = game.plane.boundaryLeft + this.collisionRadius;
                this.velocity.X = -this.velocity.X;
            }

            if (position.X + this.collisionRadius > game.plane.boundaryRight) { 
                position.X = game.plane.boundaryRight - this.collisionRadius; 
                this.velocity.X = -this.velocity.X;
            }

            if (position.Z - this.collisionRadius < game.plane.boundaryFront) { 
                position.Z = game.plane.boundaryFront + this.collisionRadius; 
                this.velocity.Z = -this.velocity.Z;
            }

            if (position.Z + this.collisionRadius > game.plane.boundaryBack) { 
                position.Z = game.plane.boundaryBack - this.collisionRadius; 
                this.velocity.Z = -this.velocity.Z;
            }

            world = Matrix.Scaling(size / scalingFactor) * Matrix.Translation(position);
            view = game.camera.View;
            collisionRadius = modelBounds.Radius * (size / scalingFactor);
            position.Y = Plane.PLANE_HT + collisionRadius + 0.1f;
        }

        // when player eat an enemy
        public void Hit(Player player)
        {
            game.score += 10;
            // update number of spheres and status (size) of spheres in enemycontroller
            game.enemycontroller.createNextEnemy();
            game.Remove(this);
            
        }
    }
}
