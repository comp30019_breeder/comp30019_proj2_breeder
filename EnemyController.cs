﻿/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

// This script controls how to spawn the enemy

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using System.Diagnostics;

namespace Project
{
    using SharpDX.Toolkit.Graphics;

    
    // Enemy Controller class.
    public class EnemyController : GameObject
    {

        // current player size
        private float playerSize;

        
        // maximum number of big and small spheres
        private int bigSphereMax, smallSphereMax;
        // current number of big and small spheres
        public int bigSphereCount, smallSphereCount, sameSphereCount;


        // radius of player's surrounding
        private static float appearRadius = 6.0f;

        

        private Random rand;

        // Constructor.
        public EnemyController(LabGame game)
        {
            this.game = game;
            
            rand = new Random();

            playerSize = game.player.size;

            // Configure number of big and small spheres to create based on game difficulty
            
            bigSphereMax = (int)(game.bigSphereRatio * game.totalSphereMax);

            smallSphereMax = game.totalSphereMax - bigSphereMax;

            // Set current number of spheres to zero
            bigSphereCount = 0;
            smallSphereCount = 0;
            sameSphereCount = 0;

            //Create enemies
            //nextWave();
        }

        // Set up the next wave.
        private void nextWave()
        {
            // create 10 enemies in the wave
            for(int i=0;i<game.totalSphereMax;i++)
            {
                createEnemy();
            }
        }

        // Create an enemy in the game
        private void createEnemy()
        {
            if(rand != null)
            {
                // get random size
                float size;
                if(smallSphereCount < smallSphereMax)
                {
                    size = generateSmallEnemy();
                    this.smallSphereCount++;
                }
                else
                {
                    size = generateBigEnemy();
                    this.bigSphereCount++;
                }
                

                // X and Z coordinate of sphere
                float x, z;

                Vector3 tmpcenter = game.player.position;

                // get random position


                // if player's sphere is near to the left edge of plane,
                // enemy sphere should appear at right edge of plane.
                if (tmpcenter.X - appearRadius <= game.plane.boundaryLeft)
                {
                    x = rand.NextFloat(tmpcenter.X + appearRadius, game.plane.boundaryRight);            
                }
                // if player's sphere is near to the right edge of plane,
                // enemy sphere should appear at left edge of plane.
                else if (tmpcenter.X + appearRadius >= game.plane.boundaryRight)
                {
                    x = rand.NextFloat(game.plane.boundaryLeft, tmpcenter.X - appearRadius);
                }
                // else choose either side of plane for X
                else
                {
                    int randX = rand.Next(2);
                    if(randX == 0)
                    {
                        x = rand.NextFloat(tmpcenter.X + appearRadius, game.plane.boundaryRight);
                    }
                    else
                    {
                        x = rand.NextFloat(game.plane.boundaryLeft, tmpcenter.X - appearRadius);
                    }
                }

                // if player's sphere is near to the front edge of plane,
                // enemy sphere should appear at back edge of plane.
                if (tmpcenter.Z - appearRadius <= game.plane.boundaryFront)
                {
                    z = rand.NextFloat(tmpcenter.Z+appearRadius, game.plane.boundaryBack);
                }
                // if player's sphere is near to the right edge of plane,
                // enemy sphere should appear at left edge of plane.
                else if (tmpcenter.Z + appearRadius >= game.plane.boundaryBack)
                {
                    z = rand.NextFloat(game.plane.boundaryFront, tmpcenter.Z-appearRadius);
                }
                // else choose either side of plane for Z
                else
                {
                    int randZ = rand.Next(2);
                    if(randZ == 0)
                    {
                        z = rand.NextFloat(tmpcenter.Z + appearRadius, game.plane.boundaryBack);
                    }
                    else
                    {
                        z = rand.NextFloat(game.plane.boundaryFront, tmpcenter.Z - appearRadius);
                    }
                }

                

                game.Add(new Enemy(game, new Vector3(x, Plane.PLANE_HT + 1.5f, z), size));
                //game.Add(new Enemy(game, new Vector3(0, Plane.PLANE_HT, 0), 1));
            }
        }

        // create new enemy when sphere

        // random size generators
        private float generateSmallEnemy()
        {
            return rand.NextFloat(MIN_SIZE - 1.0f, game.player.size - 1.0f);
        }
        private float generateBigEnemy()
        {
            return rand.NextFloat(game.player.size + 1.0f, MAX_SIZE + 1.0f);
        }



        // Frame update method.
        public override void Update(GameTime gameTime)
        {
            if (game.startedFirstTime==false)
            {

                // Configure number of big and small spheres to create based on game difficulty

                bigSphereMax = (int)(game.bigSphereRatio * game.totalSphereMax);

                smallSphereMax = game.totalSphereMax - bigSphereMax;

                nextWave();
                game.startedFirstTime = true;
            }

        }



        // Updates status of spheres and create next sphere
        // fitting the big-small ratio as much as possible

        public void createNextEnemy()
        {
            updateSizeStatus();
            createEnemy();
        }
        public void updateSizeStatus()
        {
            // Reset number of small, equal and big spheres
            this.smallSphereCount = 0;
            this.bigSphereCount = 0;
            this.sameSphereCount = 0;

            // Update status of each sphere
            foreach (var obj in game.gameObjects)
            {
                if (obj.type == GameObjectType.Enemy)
                {
                    Enemy enemy = (Enemy)obj;
                    if(enemy.size < game.player.size)
                    {
                        this.smallSphereCount++;
                    }
                    else if(enemy.size > game.player.size)
                    {
                        this.bigSphereCount++;
                    }
                    else
                    {
                        this.sameSphereCount++;
                    }
                }
            }
        }


        // Method for when the game ends.
        private void gameOver()
        {
            game.Exit();
        }
    }
}