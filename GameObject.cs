﻿/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using Windows.UI.Input;
using Windows.UI.Core;

namespace Project
{
    using SharpDX.Toolkit.Graphics;
    public enum GameObjectType
    {
        None, Player, Enemy
    }

    // Super class for all game objects.
    abstract public class GameObject
    {
        public static float MAX_SIZE = 11.0f;
        public static float MIN_SIZE = 2.5f;
        public Model model;
        public LabGame game;
        public GameObjectType type = GameObjectType.None;
        public BasicEffect basicEffect;
        public VertexInputLayout inputLayout;
        public Matrix world, view, projection;
        public Effect effect;
        public float collisionRadius;
        public BoundingSphere modelBounds;
        public float size;
        public float scalingFactor;
        public Vector3 position;
        public Vector3 velocity;
        public Color color;
        public const float DEFAULT_STEERING_MAX_FORCE = 0.01f;//0.001f;
        public const float DEFAULT_MAX_VELOCITY = 0.08f;//0.08f;
        public const float DEFAULT_MAX_SPEED = 0.05f;//0.05f;
        public const float DEFAULT_SIGHT_DISTANCE = 5f;//0.05f;
        public static Color FLEEING_ENEMY_COL = Color.Blue, SEEKING_ENEMY_COL = Color.Red, WANDERING_ENEMY_COL = Color.White, PLAYER_COL = Color.Purple;
        //public static Color SMALL_ENEMY_COL = Color.YellowGreen, BIG_ENEMY_COL = Color.Red, PLAYER_COL = Color.White;

        public abstract void Update(GameTime gametime);

        public void Draw(GameTime gametime)
        {
            if (model != null)
            {
                // Set the effect values
                effect.Parameters["World"].SetValue(world);
                effect.Parameters["Projection"].SetValue(game.camera.ProjectionRH);
                effect.Parameters["View"].SetValue(game.camera.ViewRH);
                effect.Parameters["cameraPos"].SetValue(game.camera.pos);
                Matrix WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
                effect.Parameters["worldInvTrp"].SetValue(WorldInverseTranspose);

                float dist = Vector3.Distance(game.player.position, this.position);
                if (this.type == GameObjectType.Player)
                {
                    effect.Parameters["isPlayer"].SetValue(true);
                }
                else
                {
                    effect.Parameters["isPlayer"].SetValue(false);
                }
                effect.Parameters["dist"].SetValue(dist);
                
                Vector3 lightPos;
                if (this.type == GameObjectType.Enemy || this.type == GameObjectType.Player)
                {
                    lightPos = game.player.position;
                    lightPos.Y += 5.0f;
                }
                else
                {
                    lightPos = game.player.position;
                }
                effect.Parameters["lightPntPos"].SetValue(lightPos);

                effect.Parameters["objCol"].SetValue(this.color.ToVector4());
                effect.Parameters["playerPos"].SetValue(game.player.position);
                if (this.type == GameObjectType.Player)
                {
                    effect.Parameters["isMerging"].SetValue(false);
                }
                else
                {
                    effect.Parameters["isMerging"].SetValue(game.player.isMerging);
                }
                //System.Diagnostics.Debug.WriteLine(world);
                effect.Parameters["radius"].SetValue(game.player.radius);
                effect.Parameters["intensity"].SetValue(game.player.size - 1);
                game.GraphicsDevice.SetBlendState(game.GraphicsDevice.BlendStates.AlphaBlend);
                // Draw the model
                effect.CurrentTechnique.Passes[0].Apply();
                model.Draw(game.GraphicsDevice, world, game.camera.ViewRH, game.camera.ProjectionRH, effect);
            }
        }

        public void GetParams(string modelName)
        {
            model = game.Content.Load<Model>(modelName);
            modelBounds = model.CalculateBounds();
            collisionRadius = modelBounds.Radius;

            scalingFactor = 10.0f;
            this.position = -(modelBounds.Center * (size / scalingFactor));
            position.Y = modelBounds.Radius * (size / scalingFactor);
            
            effect = game.Content.Load<Effect>("GameObjectShader");
            world = Matrix.Identity;
            view = game.camera.View;
            projection = game.camera.Projection;
        }

        // These virtual voids allow any object that extends GameObject to respond to tapped and manipulation events
        public virtual void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {

        }

        public virtual void OnManipulationStarted(GestureRecognizer sender, ManipulationStartedEventArgs args)
        {

        }

        public virtual void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {

        }

        public virtual void OnManipulationCompleted(GestureRecognizer sender, ManipulationCompletedEventArgs args)
        {

        }
    }
}
