﻿using Project.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using System.Diagnostics;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Project
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class HighScore
    {
        private MainPage parent;
        private List<Tuple<string, int, int, double>> highscorelist = new List<Tuple<string, int, int, double>>();
        public HighScore(MainPage parent)
        {
            InitializeComponent();
            this.parent = parent;

            Show_Scores();
        }

        public async void Show_Scores()
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile localFile = await storageFolder.CreateFileAsync("highscores.txt", CreationCollisionOption.OpenIfExists);

            var userContent = await FileIO.ReadLinesAsync(localFile);


            foreach (var line in userContent)
            {
                highscorelist.Add(new Tuple<string, int, int, double>(line.Split(':')[0], Int32.Parse(line.Split(':')[1]), Int32.Parse(line.Split(':')[2]), Int32.Parse(line.Split(':')[3])));
            }
            highscorelist.Sort((a, b) => a.Item2.CompareTo(b.Item2));
            highscorelist.Reverse();

            int numOfNames = 0;
            foreach (var element in highscorelist)
            {

                if (numOfNames < 10)
                {
                    scoreName.Text += element.Item1 + "\n";
                    scoreData.Text += element.Item2.ToString() + "\n";
                    noSphereData.Text += element.Item3.ToString() + "\n";
                    ratioSphereData.Text += element.Item4.ToString() + "\n";
                    numOfNames++;
                }
                else
                {
                    break;
                }

            }
        }

        private void GoBack(object sender, RoutedEventArgs e)
        {
            parent.Children.Add(parent.mainMenu);
            parent.Children.Remove(this);
        }
    }
}
