﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using SharpDX;
using SharpDX.Toolkit;
using System;
using System.Collections.Generic;
using Windows.UI.Input;
using Windows.UI.Core;
using Windows.Devices.Sensors;
using System.IO;
using Windows.Storage;
using System.Diagnostics;

namespace Project
{
    // Use this namespace here in case we need to use Direct3D11 namespace as well, as this
    // namespace will override the Direct3D11.
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    using Windows.UI.Xaml.Controls;
    

    public class LabGame : Game
    {
        private GraphicsDeviceManager graphicsDeviceManager;
        public List<GameObject> gameObjects;
        private Stack<GameObject> addedGameObjects;
        private Stack<GameObject> removedGameObjects;
        private KeyboardManager keyboardManager;
        public KeyboardState keyboardState;
        public Player player;
        public EnemyController enemycontroller;

        // max zoom out high
        public const float MAX_ZOOM_OUT = 62f;

        // max zoom in high
        public const float MAX_ZOOM_IN = 1f;
        
        // control readings
        public bool isCtrlAccel;
        public AccelerometerReading accelerometerReading;
        public GameInput input;


        public int score;
        public string userName="Anonymous";

        // the sound of player eating(merging) with an enemy
        public MediaElement mergingSoundRef;

        public MainPage mainPage;

        // maximum total number of spheres
        public int totalSphereMax;
        // difficulty of game as percentage of small spheres
        public double bigSphereRatio;

        // the sensitivity of accelerator
        public float ctrlSensitivity;

        // Use this to represent AI difficulty ( to use advance AI or not)
        public bool isAIEasy;

        // Represents the camera's position and orientation
        public Camera camera;

        // Random number generator
        public Random random;


        public bool started = false;
        public bool startedFirstTime = false;        
        public Plane plane;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="LabGame" /> class.
        /// </summary>
        public LabGame(MainPage mainPage, MediaElement mergingSound)
        {
            // Creates a graphics manager. This is mandatory.
            graphicsDeviceManager = new GraphicsDeviceManager(this);

            // Setup the relative directory to the executable directory
            // for loading contents with the ContentManager
            Content.RootDirectory = "Content";

            // Create the keyboard manager
            keyboardManager = new KeyboardManager(this);

            random = new Random();
            input = new GameInput();

            
            // Initialise event handling.
            input.gestureRecognizer.Tapped += Tapped;
            input.gestureRecognizer.ManipulationStarted += OnManipulationStarted;
            input.gestureRecognizer.ManipulationUpdated += OnManipulationUpdated;
            input.gestureRecognizer.ManipulationCompleted += OnManipulationCompleted;

            this.mainPage = mainPage;
            
            score = 0;

            totalSphereMax = 15;
            bigSphereRatio = 0.5;
            
            ctrlSensitivity = 0.2f;

            isAIEasy = true;
            isCtrlAccel = true;
            this.mergingSoundRef = mergingSound;
        }

        protected override void LoadContent()
        {
            // Initialise game object containers.
            gameObjects = new List<GameObject>();
            addedGameObjects = new Stack<GameObject>();
            removedGameObjects = new Stack<GameObject>();
            
            // Create game objects.
            plane = new Plane(this);

            player = new Player(this);
            gameObjects.Add(player);

            // enemy controller controls how to spawn enemies
            enemycontroller = new EnemyController(this);
            gameObjects.Add(enemycontroller);

            base.LoadContent();
        }

        protected override void Initialize()
        {
            Window.Title = "Breeder";
            camera = new Camera(this);

            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            if (started)
            {
                keyboardState = keyboardManager.GetState();
                if (isCtrlAccel)
                {
                    if (Accelerometer.GetDefault() != null)
                    {
                        accelerometerReading = input.accelerometer.GetCurrentReading();
                    }
                    else
                    {
                        isCtrlAccel = false;
                        ctrlSensitivity = 0.5f;
                    }
                    
                }
                


                flushAddedAndRemovedGameObjects();
                camera.Update();
                
                for (int i = 0; i < gameObjects.Count; i++)
                {
                    gameObjects[i].Update(gameTime);
                }

                mainPage.UpdateScore(score, player.size, player.shrinkTimer);

                plane.Update(gameTime);

                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    this.Exit();
                    this.Dispose();
                    App.Current.Exit();
                }
                // Handle base.Update
            }
            base.Update(gameTime);

        }

        // this method to save a score to a score file at application folder
        public async void Save_Score(int newScore)
        {

            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile file = await storageFolder.CreateFileAsync("highscores.txt", CreationCollisionOption.OpenIfExists);

            if (file != null)
            {
                try
                {
                    int tmpratio = (int)(bigSphereRatio * 100);
                    string userContent = this.userName + ':' + newScore.ToString()+ ':' + totalSphereMax + ':' + tmpratio  + '\n';
                    
                    if (!String.IsNullOrEmpty(userContent))
                    {
                        await FileIO.AppendTextAsync(file, userContent);
                       
                    }

                }
                catch (FileNotFoundException)
                {

                }
            }

        }



      
        protected override void Draw(GameTime gameTime)
        {
            if (started)
            {
                // Clears the screen with the Color.CornflowerBlue
                GraphicsDevice.Clear(Color.Black);
                //GraphicsDevice.SetBlendState(GraphicsDevice.BlendStates.Additive);
                plane.Draw(gameTime);
                for (int i = 0; i < gameObjects.Count; i++)
                {
                    gameObjects[i].Draw(gameTime);
                }
                
            }
            // Handle base.Draw
            base.Draw(gameTime);
        }
        // Count the number of game objects for a certain type.
        public int Count(GameObjectType type)
        {
            int count = 0;
            foreach (var obj in gameObjects)
            {
                if (obj.type == type) { count++; }
            }
            return count;
        }

        // Add a new game object.
        public void Add(GameObject obj)
        {
            if (!gameObjects.Contains(obj) && !addedGameObjects.Contains(obj))
            {
                addedGameObjects.Push(obj);
            }
        }

        // Remove a game object.
        public void Remove(GameObject obj)
        {
            if (gameObjects.Contains(obj) && !removedGameObjects.Contains(obj))
            {
                removedGameObjects.Push(obj);
            }
        }

        // Process the buffers of game objects that need to be added/removed.
        private void flushAddedAndRemovedGameObjects()
        {
            while (addedGameObjects.Count > 0) { gameObjects.Add(addedGameObjects.Pop()); }
            while (removedGameObjects.Count > 0) { gameObjects.Remove(removedGameObjects.Pop()); }
        }

        public void OnManipulationStarted(GestureRecognizer sender, ManipulationStartedEventArgs args)
        {
            // Pass Manipulation events to the game objects.

        }

        public void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {

        }

        public void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {
            // Zoom in and zoom out effect
            camera.pos.Y *= 2- args.Delta.Scale;

            // bound the camera with zoom out
            if (camera.pos.Y > MAX_ZOOM_OUT)
            {
                camera.pos.Y = MAX_ZOOM_OUT;
            }

            // bound the camera with zoom in
            if (camera.pos.Y < MAX_ZOOM_IN)
            {
                camera.pos.Y = MAX_ZOOM_IN;
            }
        }

        public void OnManipulationCompleted(GestureRecognizer sender, ManipulationCompletedEventArgs args)
        {
        }

        public void RestartGame()
        {
            this.LoadContent();
            this.score = 0;
        }

    }
}
