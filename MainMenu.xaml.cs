﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SharpDX;
using System;

namespace Project
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    // TASK 4: Instructions Page
    public sealed partial class MainMenu
    {
        private MainPage parent;
        public MainMenu(MainPage parent)
        {
            this.parent = parent;
            InitializeComponent();
        }
        private void StartGame(object sender, RoutedEventArgs e)
        {

            parent.StartGame();
        }

        private void LoadInstructions(object sender, RoutedEventArgs e)
        {
            parent.Children.Add(new Instructions1(parent));
            parent.Children.Remove(this);
        }

        private void LoadSettings(object sender, RoutedEventArgs e)
        {
            parent.Children.Add(new SettingGame(this.parent));
            parent.Children.Remove(this);
        }

        private void LoadHighScore(object sender, RoutedEventArgs e)
        {
            parent.Children.Add(new HighScore(this.parent));
            parent.Children.Remove(this);
        }

        private void LoadCredits(object sender, RoutedEventArgs e)
        {
            parent.Children.Add(new Credits(this.parent));
            parent.Children.Remove(this);
        }
    }
}
