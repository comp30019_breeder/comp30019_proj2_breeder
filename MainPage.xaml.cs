﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SharpDX;
using System.Diagnostics;
namespace Project
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        public LabGame game;
        public MainMenu mainMenu;
        public MediaElement menuMusicRef;
        public MediaElement mergingSoundRef;
        public MainPage()
        {

            InitializeComponent();
            mergingSoundRef = mergingSound;
            game = new LabGame(this, mergingSoundRef);
            game.Run(this);
            mainMenu = new MainMenu(this);
            this.Children.Add(mainMenu);
            menuMusicRef = menuMusic;
        }

        // TASK 1: Update the game's score
        public void UpdateScore(int score, float size, float timer)
        {
            txtScore.Text = "Score: " + score.ToString();
            txtSize.Text = "Size: " + size.ToString();
            int timerSec = (int)(timer / 1000);
            txtTimer.Text = "Size Shrinks in: " + timerSec.ToString();
        }

        // TASK 2: Starts the game.  Not that it seems easier to simply move the game.Run(this) command to this function,
        // however this seems to result in a reduction in texture quality on some machines.  Not sure why this is the case
        // but this is an easy workaround.  Not we are also making the command button invisible after it is clicked
        public void StartGame()
        {
            menuMusic.Stop();
            backgroundMusic.Play();
            this.Children.Remove(mainMenu);
            game.started = true;
        }

        public void GameOver()
        {
            game.started = false;
            backgroundMusic.Stop();
            this.Children.Add(new GameOver(this));
            game.RestartGame();
        }

        private void stopGame(object sender, RoutedEventArgs e)
        {
            this.Children.Add(new PauseMenu(this));
            
        }

        private void zoomIn(object sender, RoutedEventArgs e)
        {
            game.camera.pos.Y  -= 1f;
            if (game.camera.pos.Y < LabGame.MAX_ZOOM_IN)
            {
                game.camera.pos.Y = LabGame.MAX_ZOOM_IN;
            }
        }

        private void zoomOut(object sender, RoutedEventArgs e)
        {

            game.camera.pos.Y += 1f;
            if (game.camera.pos.Y > LabGame.MAX_ZOOM_OUT)
            {
                game.camera.pos.Y = LabGame.MAX_ZOOM_OUT;
            }
            
        }
    }
}
