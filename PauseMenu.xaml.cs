﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SharpDX;
namespace Project
{
    public sealed partial class PauseMenu : SettingsFlyout
    {
        private MainPage parent;

        public PauseMenu(MainPage parent)
        {
            InitializeComponent();
            this.parent = parent;
            parent.game.started = false;
        }

        private void ContinueGame(object sender, RoutedEventArgs e)
        {
            this.parent.game.started = true;
            parent.Children.Remove(this);
        }

        private void QuitGame(object sender, RoutedEventArgs e)
        {
            this.parent.game.player.Hit(); 
            parent.Children.Remove(this);
        }

        private void SettingsFlyout_BackClick(object sender, BackClickEventArgs e)
        {
            this.parent.game.started = true;
            parent.Children.Remove(this);
        }
    }
}
