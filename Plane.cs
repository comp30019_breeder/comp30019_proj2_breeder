﻿/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace Project
{
    using SharpDX.Toolkit.Graphics;
    public class Plane : PlaneGameObject
    {
        public static float PLANE_DIM = 50.0f;
        public static float PLANE_HT = 0f;

        public float boundaryLeft, boundaryRight, boundaryBack, boundaryFront, time;
        private Effect effect;

        private Texture2D texture, waterBumpMap;
        public Plane(LabGame game)
        {
            vertices = Buffer.Vertex.New(
                game.GraphicsDevice,
                new[]
                    {
                        new VertexPositionNormalTexture(new Vector3(PLANE_DIM/2, PLANE_HT, PLANE_DIM/2), Vector3.UnitY,new Vector2(1.0f, 1.0f)),
                        new VertexPositionNormalTexture(new Vector3(-PLANE_DIM/2, PLANE_HT, PLANE_DIM/2), Vector3.UnitY,new Vector2(0.0f, 1.0f)),
                        new VertexPositionNormalTexture(new Vector3(-PLANE_DIM/2, PLANE_HT, -PLANE_DIM/2), Vector3.UnitY, new Vector2(0.0f, 0.0f)),

                        new VertexPositionNormalTexture(new Vector3(-PLANE_DIM/2, PLANE_HT, -PLANE_DIM/2), Vector3.UnitY,new Vector2(0.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(PLANE_DIM/2, PLANE_HT, -PLANE_DIM/2), Vector3.UnitY,new Vector2(1.0f, 0.0f)),
                        new VertexPositionNormalTexture(new Vector3(PLANE_DIM/2, PLANE_HT, PLANE_DIM/2), Vector3.UnitY,new Vector2(1.0f, 1.0f))
                    }
            );

            this.boundaryLeft = -PLANE_DIM / 2;
            this.boundaryRight = PLANE_DIM / 2;
            this.boundaryBack = PLANE_DIM / 2;
            this.boundaryFront = -PLANE_DIM / 2;

            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            SetBasicEffect(game);
            this.game = game;
            this.time = 0;
            effect = game.Content.Load<Effect>("PlaneShader");

            texture = game.Content.Load<Texture2D>("skynight3.jpg");
            waterBumpMap = game.Content.Load<Texture2D>("waterbump.dds");
        }

        public override void Update(GameTime gametime)
        {
            basicEffect.View = game.camera.View;

        }

        public override void Draw(GameTime gametime)
        {
            // Set the effect values
            effect.Parameters["World"].SetValue(basicEffect.World);
            effect.Parameters["Projection"].SetValue(game.camera.ProjectionRH);
            effect.Parameters["View"].SetValue(game.camera.ViewRH);
            //System.Diagnostics.Debug.WriteLine(basicEffect.World);
            effect.Parameters["cameraPos"].SetValue(game.camera.pos);
            Matrix WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(basicEffect.World));
            effect.Parameters["worldInvTrp"].SetValue(WorldInverseTranspose);
            Vector3 lightPos = game.player.position;
            lightPos.Y += 5.0f;
            effect.Parameters["lightPntPos"].SetValue(lightPos);
            effect.Parameters["ModelTexture"].SetResource(this.texture);
            effect.Parameters["xWaterBumpMap"].SetResource(this.waterBumpMap);
            effect.Parameters["xWaveLength"].SetValue(0.1f);
            effect.Parameters["xWaveHeight"].SetValue(0.3f);

            //float time = (float)gametime.TotalGameTime.TotalMilliseconds / 10000.0f;
            if (!game.player.isMerging)
            {
                time += (float)gametime.ElapsedGameTime.TotalMilliseconds / 10000.0f;
            }
            effect.Parameters["xTime"].SetValue(time);

            effect.Parameters["xWindForce"].SetValue(1.0f);
            Vector3 windDirection = new Vector3(0, 0, 1);
            effect.Parameters["xWindDirection"].SetValue(windDirection);
            effect.Parameters["textureSampler"].SetResource(game.GraphicsDevice.SamplerStates.LinearMirror);
            effect.Parameters["playerPos"].SetValue(game.player.position);
            effect.Parameters["isMerging"].SetValue(game.player.isMerging);

            effect.Parameters["radius"].SetValue(game.player.radius);
            effect.Parameters["intensity"].SetValue(game.player.size/2.0f);
            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);

            // Apply the basic effect technique and draw the rotating cube
            //effect.CurrentTechnique.Passes[0].Apply();
            //basicEffect.CurrentTechnique.Passes[0].Apply();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
            }
        }
    }

}
