﻿/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace Project
{
    using SharpDX.Toolkit.Graphics;
    abstract public class PlaneGameObject
    {
        public BasicEffect basicEffect;
        public VertexInputLayout inputLayout;
        public LabGame game;

        public void SetBasicEffect(LabGame game)
        {
            basicEffect = new BasicEffect(game.GraphicsDevice)
            {
                View = game.camera.View,
                Projection = game.camera.Projection,
                World = Matrix.Identity,
                VertexColorEnabled = true
            };
        }
        public Buffer<VertexPositionNormalTexture> vertices;

        public abstract void Update(GameTime gametime);
        public abstract void Draw(GameTime gametime);
    }
}
