﻿/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using Windows.UI.Input;
using Windows.UI.Core;
using System.Diagnostics;

namespace Project
{
    using SharpDX.Toolkit.Graphics;
    using SharpDX.Toolkit.Input;
    // Player class to controls player character
    

    public class Player : GameObject
    {
        private static string modelName = "sphere";
        public static float PLAYER_STARTING_SIZE = 4.0f;
        public const float PLAYER_STEERING_MAX_FORCE = 0.1f;
        public const float PLAYER_MAX_VELOCITY = 0.8f;
        public const float PLAYER_MAX_SPEED = 0.5f;
        public const float PLAYER_FRICTION = 0.98f;
        public const float PLAYER_SIGHT_DISTANCE = 3f;


        // when the player eating an enemy
        public bool isMerging;

        public float radius;
        private bool decrease_radius;
        // Timer to shrink player's spheresize
        public float shrinkTimer = 0;
        private float shrinkWait = 20000;


        public Player(LabGame game)
        {
            this.velocity = new Vector3(0, 0, 0);
            this.game = game;
            this.size = PLAYER_STARTING_SIZE;
            type = GameObjectType.Player;
            GetParams(modelName);

            // set player to that color
            this.color = GameObject.PLAYER_COL;
            isMerging = false;
            radius = 0;
            decrease_radius = false;
            shrinkTimer = shrinkWait;
            
        }
        
        // Frame update.
        public override void Update(GameTime gameTime)
        {
           
            // Game is paused when merging
            if (!isMerging)
            {
                Vector3 steeringForce = new Vector3(0, 0, 0);

                
                if(game.isCtrlAccel)
                {
                    // Control for Tablet
                    steeringForce.X =- (Player.PLAYER_STEERING_MAX_FORCE) * (float)game.accelerometerReading.AccelerationX * 10 * game.ctrlSensitivity;
                    steeringForce.Z = (Player.PLAYER_STEERING_MAX_FORCE) * ((float)game.accelerometerReading.AccelerationY) * 10 * game.ctrlSensitivity;
                }
                else
                {
                    // Control for Keyboard
                    if (game.keyboardState.IsKeyDown(Keys.Left)) { steeringForce.X += (Player.PLAYER_STEERING_MAX_FORCE) * game.ctrlSensitivity; }
                    if (game.keyboardState.IsKeyDown(Keys.Right)) { steeringForce.X -= (Player.PLAYER_STEERING_MAX_FORCE) * game.ctrlSensitivity; }
                    if (game.keyboardState.IsKeyDown(Keys.Up)) { steeringForce.Z += (Player.PLAYER_STEERING_MAX_FORCE) * game.ctrlSensitivity; }
                    if (game.keyboardState.IsKeyDown(Keys.Down)) { steeringForce.Z -= (Player.PLAYER_STEERING_MAX_FORCE) * game.ctrlSensitivity; }
                }

                // truncated to ensure it will not exceed the amount of allowed forces the character can handle.
                steeringForce = SteeringManager.truncate(steeringForce, Player.PLAYER_STEERING_MAX_FORCE);
                steeringForce = steeringForce / this.size;

                this.velocity = SteeringManager.truncate(this.velocity + steeringForce, Player.PLAYER_MAX_SPEED);

                // reduce the force with friction
                this.velocity *= PLAYER_FRICTION;

                this.position += this.velocity;


                //Keep player within the boundaries.
                if (position.X - this.collisionRadius < game.plane.boundaryLeft)
                {
                    position.X = game.plane.boundaryLeft + this.collisionRadius;
                    this.velocity.X = -this.velocity.X;
                }
                if (position.X + this.collisionRadius > game.plane.boundaryRight)
                {
                    position.X = game.plane.boundaryRight - this.collisionRadius;
                    this.velocity.X = -this.velocity.X;
                }
                if (position.Z - this.collisionRadius < game.plane.boundaryFront)
                {
                    position.Z = game.plane.boundaryFront + this.collisionRadius;
                    this.velocity.Z = -this.velocity.Z;
                }
                if (position.Z + this.collisionRadius > game.plane.boundaryBack)
                {
                    position.Z = game.plane.boundaryBack - this.collisionRadius;
                    this.velocity.Z = -this.velocity.Z;
                }

                // Shrink size after 10 seconds
                shrinkTimer -= gameTime.ElapsedGameTime.Milliseconds;
                if (shrinkTimer <= 0)
                {
                    shrinkSize();
                    shrinkTimer = shrinkWait;
                }

                checkForCollisions();

                // Shrink size after 20 seconds
                shrinkTimer -= gameTime.ElapsedGameTime.Milliseconds;
                if (shrinkTimer <= 0)
                {
                    shrinkSize();
                    shrinkTimer = shrinkWait;
                }
            }
            else
            {
                if (!decrease_radius)
                {
                    radius += 1.0f;
                }
                else
                {
                    radius -= 1.0f;
                }

                if(radius >= Plane.PLANE_DIM/2.0f)
                {
                    decrease_radius = true;
                }

                if (decrease_radius && radius == 0)
                {
                    radius = 0;
                    decrease_radius = false;
                    isMerging = false;
                    //reset shrink timer
                    shrinkTimer = shrinkWait;
                }
            }


            world = Matrix.Scaling(size / scalingFactor) * Matrix.Translation(position);
            view = game.camera.View;

            collisionRadius = modelBounds.Radius * (size / scalingFactor);
            position.Y = Plane.PLANE_HT + collisionRadius;
        }

        private void shrinkSize()
        {
            if (size >= MIN_SIZE)
            {
                size-=0.5f;
            }
        }

        // Check if collided with enemy.
        private void checkForCollisions()
        {
            foreach (var obj in game.gameObjects)
            {
                // collision checking
                if (obj.type == GameObjectType.Enemy && ((((GameObject)obj).position - position).LengthSquared() <=
                    Math.Pow(((GameObject)obj).collisionRadius + 
                    this.collisionRadius, 2)))
                {
                    // Cast to object class and call Hit method.
                    if(((Enemy)obj).size < this.size)
                    {
                        ((Enemy)obj).Hit(this);
                        game.mergingSoundRef.Play();
                        isMerging = true;
                        if (this.size < MAX_SIZE)
                        {
                            this.size+=0.5f;
                        }
                    }
                    else
                    {
                        Hit();
                    }

                }
            }
        }

        // React to getting hit by an enemy 
        public void Hit()
        {         
            // save the score
            game.Save_Score(game.score);

            //display game over screen
            game.mainPage.GameOver();
            game.startedFirstTime = false;
        }

        


        public override void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {
        }

        public override void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {
        }
    }
}