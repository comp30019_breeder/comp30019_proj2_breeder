﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SharpDX;

namespace Project
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingGame
    {
        private MainPage parent;

        public SettingGame(MainPage parent)
        {
            InitializeComponent(); 
            this.parent = parent;

            // retrieve previous game states:
            if (parent.game != null)
            {
                // Sphere Number
                sldNumOfSpheres.Value = parent.game.totalSphereMax;

                // % of Small Sphere
                sldSphereRatio.Value = parent.game.bigSphereRatio * 100;

                // AI Difficulty
                if(parent.game.isAIEasy)
                {
                    aiEasy.IsChecked = true;
                }
                else
                {
                    aiHard.IsChecked = true;
                }
            }

                    
        } 

        private void GoSettingOthers(object sender, RoutedEventArgs e)
        {
            parent.Children.Add(new SettingOthers(this.parent));
            parent.Children.Remove(this);
        }

        private void changeSphereNum(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (parent != null) { parent.game.totalSphereMax = (int)e.NewValue; }
        }

        private void changeSmallRatio(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (parent != null) { parent.game.bigSphereRatio = (e.NewValue/100.0); }
        }

        private void setdiffBeginner(object sender, RoutedEventArgs e)
        {
            // Easy AI difficulty
            this.aiEasy.IsChecked = true;
            // Minimum number of spheres
            this.sldNumOfSpheres.Value = this.sldNumOfSpheres.Minimum;
            // Maximum percentage of small spheres
            this.sldSphereRatio.Value = this.sldSphereRatio.Minimum;

            // apply settings to game
            if (parent.game != null)
            {
                parent.game.isAIEasy = true;
                parent.game.totalSphereMax = (int)this.sldNumOfSpheres.Value;
                parent.game.bigSphereRatio = this.sldSphereRatio.Value/100.0f;
            }
        }

        private void setdiffIntermediate(object sender, RoutedEventArgs e)
        {
            // Hard AI difficulty
            this.aiHard.IsChecked = true;
            // Medium number of spheres
            this.sldNumOfSpheres.Value = 40;
            // Medium percentage of small spheres
            this.sldSphereRatio.Value = 40;

            // apply settings to game
            if (parent.game != null)
            {
                parent.game.isAIEasy = false;
                parent.game.totalSphereMax = (int)this.sldNumOfSpheres.Value;
                parent.game.bigSphereRatio = this.sldSphereRatio.Value/100.0;
            }
        }

        private void setdiffChaos(object sender, RoutedEventArgs e)
        {
            // Hard AI difficulty
            this.aiHard.IsChecked = true;
            // Maximum number of spheres
            this.sldNumOfSpheres.Value = this.sldNumOfSpheres.Maximum;
            // Minimum small sphere percentage
            this.sldSphereRatio.Value = this.sldSphereRatio.Maximum;

            // apply settings to game
            if(parent.game != null)
            {
                parent.game.isAIEasy = false;
                parent.game.totalSphereMax = (int)this.sldNumOfSpheres.Value;
                parent.game.bigSphereRatio = this.sldSphereRatio.Value/100.0;
            }
        }

        private void aiEasy_Checked(object sender, RoutedEventArgs e)
        {
            if (parent != null) { parent.game.isAIEasy = true; }
        }

        private void aiHard_Checked(object sender, RoutedEventArgs e)
        {
            if (parent != null) { parent.game.isAIEasy = false; }
        }

        private void GoBack(object sender, RoutedEventArgs e)
        {
            parent.Children.Add(parent.mainMenu);
            parent.Children.Remove(this);
        }
    }
}
