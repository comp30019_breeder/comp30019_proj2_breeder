﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Windows.UI.Input;
using SharpDX;
using System.Diagnostics;
using Windows.Devices.Sensors;

namespace Project
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingOthers
    {
        private MainPage parent;


        public SettingOthers(MainPage parent)
        {
            InitializeComponent();
            this.parent = parent;
            
            // retrieve previous game states:
            if (parent != null) 
            {
                // User Name into textbox
                txtUsername.Text = parent.game.userName;

                // Check controls
                if(parent.game.isCtrlAccel)
                {
                    ctrlAccel.IsChecked = true;
                    sensitivityText.Text = "Accelerometer Sensitivity";
                }
                else
                {
                    ctrlKey.IsChecked = true;
                    sensitivityText.Text = "Keyboard Sensitivity";
                }

                ctrlSensitivity.Value = parent.game.ctrlSensitivity;

                if (Accelerometer.GetDefault() == null)
                {
                    ctrlAccel.Visibility = Visibility.Collapsed;
                    accelText.Text = "No Accelerometer found !";
                    ctrlKey.IsChecked = true;
                }

            }       
        }

        private void GoSettingGame(object sender, RoutedEventArgs e)
        {
            if (parent != null) { parent.game.userName = txtUsername.Text; }
            parent.Children.Add(new SettingGame(this.parent));
            parent.Children.Remove(this);
        }

        private void ctrlAccel_Checked(object sender, RoutedEventArgs e)
        {
            if (parent != null) { parent.game.isCtrlAccel = true; }
        }

        private void ctrlKey_Checked(object sender, RoutedEventArgs e)
        {
            if (parent != null) { parent.game.isCtrlAccel = false; }
        }

        private void changeSensitivity(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (parent != null) { parent.game.ctrlSensitivity = (float)e.NewValue; }
        }

        private void GoBack(object sender, RoutedEventArgs e)
        {
            if (parent != null) { parent.game.userName = txtUsername.Text; }
            parent.Children.Add(parent.mainMenu);
            parent.Children.Remove(this);
        }

        private void ctrlAccel_Click(object sender, RoutedEventArgs e)
        {
            sensitivityText.Text = "Accelerometer Sensitivity";
           
        }

        private void ctrlKey_Click(object sender, RoutedEventArgs e)
        {
            sensitivityText.Text = "Keyboard Sensitivity";
        }


    }
}
