﻿/* COMP30019 Graphics and Interaction
 * Project 2 - Breeder game
 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027
 */

// This class will control AI behavior of enemies with 5 main behaviors : flee, seek, wander, pursuit and evade
// the class enable an enemy to have multiple behaviors at the same time.

// Reference : + Ian Millington, John Funge - Artificial Intelligence for Games, 2nd edition book
//             + Fernando Bevilacqua - Understanding Steering Behaviors: Movement Manager

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using System.Diagnostics;



namespace Project
{
    class SteeringManager
    {
        // how much the wander circle radius -> affect how much enemy turn for wandering
        public const float WANDER_CIRCLE_RADIUS = 0.003f;

        // how much the wander circle distance from enemies -> affect how much enemy wander based on current velocity
        public const float WANDER_CIRCLE_DISTANCE = 0.001f;

        // how much wide the enemy will turn to wander 
        public const float WANDER_ANGLE_CHANGE = 10f;
        public Vector3 steering;
        public GameObject host;
        public float wanderAngle;
        Random random;
        LabGame game;

        public SteeringManager(GameObject host, LabGame game)
        {
            this.host = host;
            this.steering = new Vector3(0, 0, 0);
            this.wanderAngle = 0;
            random = new Random();
            this.game = game;
        }

        // The public API for seeking
        public void seek(Vector3 target)
        {
            steering += doSeek(target);
        }

        // The public API for pursuiting
        public void pursuit(Vector3 targetPos, Vector3 targetVelocity)
        {
            doPursuit(targetPos, targetVelocity);
        }

        // The public API for evading
        public void evade(Vector3 targetPos, Vector3 targetVelocity)
        {
            doEvade(targetPos, targetVelocity);
        }

        // The public API for fleeing
        public void flee(Vector3 target)
        {
            steering += doFlee(target);
        }

        // The public API for wandering
        public void wander()
        {
            steering += doWander();

        }

        // The update method will run after all of the behaviors of a enemy invoke
        //(to sum up all the forces)
        public void update()
        {

            steering = truncate(steering, GameObject.DEFAULT_STEERING_MAX_FORCE);
            steering = steering / host.size;
            //The velocity vector can be truncated to ensure it will not be greater than a certain value
            host.velocity = truncate(host.velocity + steering, GameObject.DEFAULT_MAX_SPEED);
            host.position += host.velocity;
        }

        // Reset the internal steering force.
        public void reset()
        {
            this.steering = new Vector3(0, 0, 0);
        }

        // The internal API for seeking
        private Vector3 doSeek(Vector3 target)
        {
            //The desired velocity is a force that guides the character towards its target using the shortest path possible
            Vector3 desired = Vector3.Normalize(target - host.position) * GameObject.DEFAULT_MAX_VELOCITY;

            //The steering force is the result of the desired velocity subtracted by the current velocity 
            // and it pushes the character towards the target as well. 
            Vector3 force = desired - host.velocity;

            return force;
        }

        // The internal API for pursuiting
        private void doPursuit(Vector3 targetPos, Vector3 targetVelocity)
        {
            //distance between the two characters and the maximum velocity the target can achieve
            float predictRate = Vector3.Distance(targetPos, host.position) / GameObject.DEFAULT_MAX_VELOCITY;
            Vector3 futurePos = targetPos + targetVelocity * predictRate;

            seek(futurePos);
        }

        // The internal API for evading
        private void doEvade(Vector3 targetPos, Vector3 targetVelocity)
        {
            //distance between the two characters and the maximum velocity the target can achieve
            float predictRate = Vector3.Distance(targetPos, host.position) / GameObject.DEFAULT_MAX_VELOCITY;
            Vector3 futurePos = targetPos + targetVelocity * predictRate;

            flee(futurePos);
        }

        // The internal API for fleeing
        private Vector3 doFlee(Vector3 target)
        {
            //The desired velocity is a force that guides the character towards its target using the shortest path possible
            Vector3 desired = Vector3.Normalize(host.position - target) * GameObject.DEFAULT_MAX_VELOCITY;

            //The steering force is the result of the desired velocity subtracted by the current velocity 
            // and it pushes the character towards the target as well. 
            Vector3 force = desired - host.velocity;
            return force;
        }

        // The internal API for wandering
        private Vector3 doWander()
        {

            // Calculate the circle center
            Vector3 circleCenter;
            circleCenter = new Vector3(host.velocity.X, host.velocity.Y, host.velocity.Z);
            circleCenter = Vector3.Normalize(circleCenter);
            circleCenter = circleCenter * WANDER_CIRCLE_DISTANCE;

            // Calculate the displacement force
            Vector3 displacement;
            displacement = new Vector3(0, 0, 1);
            displacement = Vector3.Normalize(displacement);
            displacement = displacement * WANDER_CIRCLE_RADIUS;


            // Randomly change the vector direction by making it change its current angle
            wanderAngle += game.random.NextFloat(-1.0f, 1.0f) * WANDER_ANGLE_CHANGE;

            // Change wanderAngle just a bit, so it won't have the same value in the next game frame.
            displacement = setAngle(displacement, wanderAngle);


            // Finally calculate and return the wander force
            Vector3 wanderForce;
            wanderForce = circleCenter + displacement;
            return wanderForce;
        }

        // truncated to ensure it will not exceed the amount of allowed forces the character can handle.
        public static Vector3 truncate(Vector3 vector, float max)
        {
            float i;
            i = max / vector.Length();
            if (i >= 1.0)
            {
                i = 1.0f;
            }
  
            vector *= i;
            return vector;
        }

        // give a new vector based on the input vector and a input degree to turn that vector
        public Vector3 setAngle(Vector3 vector, float degree)
        {

            float len = vector.Length();

            double radian = (Math.PI * degree / 180.0);

            vector.X = (float)(Math.Cos(radian) * len);

            vector.Z = (float)(Math.Sin(radian) * len);

            return vector;
        }
    }
}
