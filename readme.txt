COMP30019 Graphics and Interaction

Project Name: Breeder

 * Name : Jason Lee Sui Yuen - 591740
 *        Nguyen Minh Thong Huynh - 598093
 *        Seunghwa Kang - 602027

What the application does
This is a survival game which runs on Windows 8 using DirectX.
The application makes use of keyboard input, touch input and accelerometers.

How to use it
Run the game from the Visual Studio.
At the main menu, click on 'How to Play' to have a quick understanding of how the game works.
Click on 'Settings' to configure the properties for the game.
Click on 'Play' to play the game.


How we model objects
We used Blender to import and model spheres for both the player and enemies. 
Plane was created by hard-coding the vertices.


How we handle graphics and camera motion
Lightings: 2 customised shaders are used for this game.
			- 1 for plane
			- 1 for Gameobjects (player and enemy spheres)

Camera Motion: In the game, the camera is changed based on the player's sphere so that the camera is following the player.
The distance between the camera and the plane also changes using the touch input and/or zoom in buttons on the game page.



References

Project based on project 2 framework given.

Water waves technique adapted from XNA tutorials from:
http://www.riemers.net/eng/Tutorials/XNA/Csharp/Series4/Ripples.php
http://www.riemers.net/eng/Tutorials/XNA/Csharp/Series4/Moving_water.php

Bump Map(waterbump.dds) downloaded from:
http://www.riemers.net/eng/Tutorials/XNA/Csharp/Series4/Ripples.php

AI behaviour of enemies technique adapted from:
Ian Millington, John Funge - Artificial Intelligence for Games, 2nd edition book
Fernando Bevilacqua - Understanding Steering Behaviours: Movement Manager

Note: App certification is in the project folder named “certificate.xml”